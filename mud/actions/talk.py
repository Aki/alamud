from .action import Action2
from mud.events import TalkEvent

class TalkAction(Action2):
    EVENT = TalkEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "talk"